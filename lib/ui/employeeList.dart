import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import '../models/EmployeeModels.dart';

final empReference =
    FirebaseDatabase.instance.reference().child('employeeDetails');
class employeeList extends StatefulWidget {
  
  Employee employee;
  
  @override
  employeeListState createState() => employeeListState();
}

class employeeListState extends State<employeeList> {
 
  List<Employee> empOption;
  StreamSubscription<Event> _onEmpAddedSubscription;
  StreamSubscription<Event> _onEmpChangedSubscription;
  @override
    void initState() {
       empOption = new List();
      _onEmpAddedSubscription =
          empReference.onChildAdded.listen(_onEmpAdded);
      _onEmpChangedSubscription =
          empReference.onChildChanged.listen(_onEmpUpdated);
      // TODO: implement initState
      super.initState();
    }
    @override
  void dispose() {
    _onEmpAddedSubscription.cancel();
    _onEmpChangedSubscription.cancel();
    
    super.dispose();
  }

  void _onEmpAdded(Event event) {
    setState(() {
      print(">>>>>>>>>>>>>> FROM ONPOLLAdded from HOME>>>>>>>>>>>>>>>>>>>>>>");
     empOption.add(new Employee.fromSnapshot(event.snapshot));
    });
  }
   void _onEmpUpdated(Event event) {

   }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('CRUD Operation Using Rest'),
        backgroundColor: Colors.deepOrange,
      ),
      body:Center(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            //color: Colors.redAccent,
            image: DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.2), BlendMode.dstOver),
              image: AssetImage('assets/background.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: <Widget>[
              FlatButton(
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(0.0)),
                color: Colors.white,
                onPressed: () => {},
                child: new Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20.0,
                    horizontal: 30.0,
                  ),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('Employee List'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 2.0,
              ),
              SizedBox(
                height: 2.0,
              ),
              new Expanded(
                  child: new ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: <Widget>[
                      SizedBox(
                        height: 10.0,
                      ),
                      ListTile(
                        title: Text(
                          empOption[index].empName,
                          style: TextStyle(
                            fontSize: 20.0,
                            color: index % 2 == 0 ? Colors.white : Colors.white,
                          ),
                        ),
                        subtitle: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            // InkWell(
                            //     child: new Container(
                            //       height: 250.0,
                            //       decoration: BoxDecoration(
                            //         //color: Colors.redAccent,
                            //         image: DecorationImage(
                            //           colorFilter: new ColorFilter.mode(
                            //               Colors.black.withOpacity(0.2),
                            //               BlendMode.dstOver),
                            //           image: NetworkImage(
                            //               winnerOptionRev[index].winnerImage),
                            //           fit: BoxFit.cover,
                            //         ),
                            //       ),
                            //       padding: const EdgeInsets.symmetric(
                            //         vertical: 20.0,
                            //         horizontal: 20.0,
                            //       ),
                            //     ),
                            //     onTap: () {
                            //     //  Navigator.of(context).pop();
                            //       Navigator.push(
                            //         context,
                            //         MaterialPageRoute(
                            //             builder: (context) => VisitWebsite(
                            //                 winnerOptionRev[index]
                            //                     .winnerName
                            //                     .toUpperCase(),
                            //                 winnerOptionRev[index]
                            //                     .winnerUrlName)),
                            //       );
                            //     }),
                            new Container(
                                child: new Row(children: <Widget>[
                              new Expanded(
                                  child: new Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        empOption[index].designation,
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          color: index % 2 == 0
                                              ? Colors.white
                                              : Colors.white,
                                        ),
                                      ),
                                      Text(
                                        empOption[index].basic,
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          color: index % 2 == 0
                                              ? Colors.white
                                              : Colors.white,
                                        ),
                                      ),
                                      Text(
                                        '',
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          color: index % 2 == 0
                                              ? Colors.white
                                              : Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ))
                            ])),
                            DecoratedBox(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.deepOrange, width: 1.0),
                                  borderRadius: BorderRadius.circular(4.0)),
                            )
                          ],
                        ),
                        onTap: () {},
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Divider(
                        color: Colors.deepOrange,
                      )
                    ],
                  );
                },
                itemCount: empOption.length,
              )),

              SizedBox(height: 100.0),
            ],
          ),
        ),
      ),
    );
  }
}
