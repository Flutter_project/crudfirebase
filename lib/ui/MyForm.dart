import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import '../ui/employeeList.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyForm(),
  ));
}

class MyForm extends StatefulWidget {
  @override
  MyFormState createState() => MyFormState();
}

class MyFormState extends State<MyForm> {
  final Map<dynamic, dynamic> _formData = {
    'empName': '',
    'city': '',
    'contactNo': '',
    'designation': '',
    'basic': '',
  };
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _nameRTextController = TextEditingController();

  Widget _buildEmpNameTextField() {
    if (_nameRTextController.text.trim() != null)
      _nameRTextController.text = _nameRTextController.text;

    return new TextFormField(
      controller: _nameRTextController,
      decoration: const InputDecoration(
        hintText: 'Enter Emp Name',
        labelText: 'Name',
      ),
      validator: (String value) {
        if (value.isEmpty || value.length < 5) {
          return 'Name can not be empty and should be 5+characters long.';
        }
      },
      onSaved: (String value) {
        _formData['empName'] = value;
      },
    );
  }

  final _cityTextController = TextEditingController();
  Widget _buildCityTextField() {
    if (_cityTextController.text.trim() != null)
      _cityTextController.text = _cityTextController.text;

    return TextFormField(
      controller: _cityTextController,
      decoration:
          const InputDecoration(hintText: 'Enter City', labelText: 'City'),
      validator: (String value) {
        if (value.isEmpty) {
          return 'City can not be empty';
        }
      },
      onSaved: (String value) {
        _formData['city'] = value;
      },
    );
  }

  final _contactTextController = TextEditingController();
  Widget _buildContactNoTextField() {
    if (_contactTextController.text.trim() != null)
      _contactTextController.text = _contactTextController.text;
    return TextFormField(
      controller: _contactTextController,
      decoration: const InputDecoration(
          hintText: 'Enter Contact No', labelText: 'Contact No'),
      validator: (String value) {
        if (value.isEmpty || value.length < 10 || value.length > 10)
          return 'Contact No can not be empty or must be 10 characters long';
      },
      onSaved: (String value) {
        _formData['contactNo'] = value;
      },
    );
  }

  final _designationTextController = TextEditingController();
  Widget _buildDesignationTextField() {
    if (_designationTextController.text.trim() != null)
      _designationTextController.text = _designationTextController.text;
    return TextFormField(
      controller: _designationTextController,
      decoration: const InputDecoration(
        hintText: 'Enter Designation',
        labelText: 'Designation',
      ),
      validator: (String value) {
        if (value.isEmpty) return 'Designation can not blank';
      },
      onSaved: (String value) {
        _formData['designation'] = value;
      },
    );
  }

  final _basicTextController = TextEditingController();
  Widget _buildBasicTextField() {
    if (_basicTextController.text.trim() != null)
      _basicTextController.text = _basicTextController.text;

    return TextFormField(
      controller: _basicTextController,
      decoration: const InputDecoration(
          hintText: 'Enter Basic Salary', labelText: 'Basic Salary'),
      validator: (String value) {
        if (value.isEmpty) return 'Basic salary can not empty';
      },
      onSaved: (String value) {
        _formData['basic'] = value;
      },
    );
  }

  void saveData() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    final _data = {
      'empName': _nameRTextController.text,
      'city': _cityTextController.text,
      'contactNo': _contactTextController.text,
      'designation': _designationTextController.text,
      'basic': _basicTextController.text
    };
    final employeeReference =
        FirebaseDatabase.instance.reference().child('employeeDetails');
    print("userReference$employeeReference");

    employeeReference.push().set({
      'empName': _nameRTextController.text,
      'city': _cityTextController.text,
      'contactNo': _contactTextController.text,
      'designation': _designationTextController.text,
      'basic': _basicTextController.text
    }).then((value) {
      showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Success'),
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[
                  new Text('Successfully insert the data.'),
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                  _nameRTextController.text = '';
                  _cityTextController.text = '';
                  _contactTextController.text = '';
                  _designationTextController.text = '';
                  _basicTextController.text = '';
                },
              ),
            ],
          );
        },
      );
    }).catchError((onError) {
      print("ErrorDisplay$onError");
    });

    print("mydata$_data");
  }

  void viewData() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => employeeList(),
      ),
    );
  }

  void updateData() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    FirebaseDatabase.instance
        .reference()
        .child('employeeDetails')
        .orderByChild('empName')
        .equalTo(_nameRTextController.text)
        .once()
        .then((DataSnapshot donationSnapshot) {
      print("donationSnapshot ${donationSnapshot.value}");
      if (donationSnapshot.value != null) {
        donationSnapshot.value.forEach((dynamic empId, dynamic empData) {
          print(empData['designation']);
          print(empId);

          final pollReferenceStatus = FirebaseDatabase.instance
              .reference()
              .child('employeeDetails/' + empId);

          pollReferenceStatus.update({
            'city': _cityTextController.text,
            'contactNo': _contactTextController.text,
            'designation': _designationTextController.text,
            'basic': _basicTextController.text
          }).then((_) {
            showDialog<Null>(
              context: context,
              barrierDismissible: false, // user must tap button!
              builder: (BuildContext context) {
                return new AlertDialog(
                  title: new Text('Success'),
                  content: new SingleChildScrollView(
                    child: new ListBody(
                      children: <Widget>[
                        new Text('Successfully Update the record'),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      child: new Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        _nameRTextController.text = '';
                        _cityTextController.text = '';
                        _contactTextController.text = '';
                        _designationTextController.text = '';
                        _basicTextController.text = '';
                      },
                    ),
                  ],
                );
              },
            );
          });
        });
      }
    });
  }

  void deleteData() {
    FirebaseDatabase.instance
        .reference()
        .child('employeeDetails')
        .orderByChild('empName')
        .equalTo(_nameRTextController.text)
        .once()
        .then((DataSnapshot donationSnapshot) {
      print("donationSnapshot ${donationSnapshot.value}");
      if (donationSnapshot.value != null) {
        donationSnapshot.value.forEach((dynamic empId, dynamic empData) {
          print(empData['designation']);
          print(empId);

          final pollReferenceStatus = FirebaseDatabase.instance
              .reference()
              .child('employeeDetails/' + empId);

          pollReferenceStatus.remove().then((_) {
            showDialog<Null>(
              context: context,
              barrierDismissible: false, // user must tap button!
              builder: (BuildContext context) {
                return new AlertDialog(
                  title: new Text('Success'),
                  content: new SingleChildScrollView(
                    child: new ListBody(
                      children: <Widget>[
                        new Text('Successfully Deleet the record'),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      child: new Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        _nameRTextController.text = '';
                        _cityTextController.text = '';
                        _contactTextController.text = '';
                        _designationTextController.text = '';
                        _basicTextController.text = '';
                      },
                    ),
                  ],
                );
              },
            );
          }).catchError((onError) {
            showDialog<Null>(
              context: context,
              barrierDismissible: false, // user must tap button!
              builder: (BuildContext context) {
                return new AlertDialog(
                  title: new Text('Error'),
                  content: new SingleChildScrollView(
                    child: new ListBody(
                      children: <Widget>[
                        new Text('Not Deleted'),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      child: new Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        _nameRTextController.text = '';
                        _cityTextController.text = '';
                        _contactTextController.text = '';
                        _designationTextController.text = '';
                        _basicTextController.text = '';
                      },
                    ),
                  ],
                );
              },
            );
          });
        });
      } else {
        showDialog<Null>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return new AlertDialog(
              title: new Text('Error'),
              content: new SingleChildScrollView(
                child: new ListBody(
                  children: <Widget>[
                    new Text('Given Emp name is not found'),
                  ],
                ),
              ),
              actions: <Widget>[
                new FlatButton(
                  child: new Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _nameRTextController.text = '';
                    _cityTextController.text = '';
                    _contactTextController.text = '';
                    _designationTextController.text = '';
                    _basicTextController.text = '';
                  },
                ),
              ],
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('CRUD Operation Using Firebase Reference'),
        backgroundColor: Colors.deepOrange,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            key: _formKey,
            child: new ListView(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              children: <Widget>[
                _buildEmpNameTextField(),
                _buildCityTextField(),
                _buildContactNoTextField(),
                _buildDesignationTextField(),
                _buildBasicTextField(),
                SizedBox(height: 15.0),
                new Container(
                  child: new RaisedButton(
                    color: Colors.deepOrange,
                    child: const Text('SUBMIT'),
                    textColor: Colors.white,
                    onPressed: () => saveData(),
                  ),
                ),
                RaisedButton(
                  color: Colors.deepOrange,
                  child: Text('View'),
                  textColor: Colors.white,
                  onPressed: () => viewData(),
                ),
                RaisedButton(
                  color: Colors.deepOrange,
                  child: Text('Update'),
                  textColor: Colors.white,
                  onPressed: () => updateData(),
                ),
                RaisedButton(
                  color: Colors.deepOrange,
                  child: Text("Delete"),
                  textColor: Colors.white,
                  onPressed: () => deleteData(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
