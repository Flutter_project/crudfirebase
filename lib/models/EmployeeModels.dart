import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class Employee {
  String _empName;
  String _contactNo;
  String _city;
  String _designation;
  String _basic;
  String _id;
 
  Employee(this._id, this._empName,this._contactNo,this._city,this._basic,this._designation);

  Employee.map(dynamic obj) {
    this._id = obj['id'];
    this._empName = obj['empName'];
    this._contactNo = obj['contactNo'];
    this._city = obj['city'];
    this._designation = obj['designation'];
    this._basic = obj['basic'];
    
    
  }

  String get id => _id;
  String get empName => _empName;
  String get contactNo => _contactNo;
  String get city => _city;
   String get designation => _designation;
  String get basic => _basic;
 
  

  Employee.fromSnapshot(DataSnapshot snapshot) {
    _id = snapshot.key;
    _empName = snapshot.value['empName'];
     _contactNo = snapshot.value['contactNo'];
      _city = snapshot.value['city'];
       _designation = snapshot.value['designation'];
      _basic = snapshot.value['basic'];
   
    
  
  }
}
